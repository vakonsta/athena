#include "../xAODConfigSvc.h"
#include "../xAODMenuWriter.h"
#include "../xAODMenuReader.h"
#include "TrigConfxAOD/xAODConfigTool.h"

DECLARE_COMPONENT( TrigConf::xAODConfigSvc )
DECLARE_COMPONENT( TrigConf::xAODMenuWriter )
DECLARE_COMPONENT( TrigConf::xAODMenuReader )
DECLARE_COMPONENT( TrigConf::xAODConfigTool )

